<img src="readme/img/thumbnail.png" align="center" title="Murrengan network"/>

[murrengan.ru](https://murrengan.ru/)

<a href="readme/en"><img src="readme/img/united_states_of_america_usa.png" height="25" width="30" title="English"></a>

### Этот репозиторий содержит код социальной сети Мурренган
Разработка ведется через [fork](https://git-scm.com/book/ru/v2/GitHub-Внесение-собственного-вклада-в-проекты) и пулл реквесты

### Текущие фичи

* Регистрация клиента. Подтверждение по почте и другие социальные сети
* Создание текстовых карточек - мурров
* Добавление и обрезка фото
* Древовидные комментарии
* Рейтинговая система лайк/дизлайк
* Чат и вебсокеты

### В разработке применяется:

* [Python 3.7.6](https://www.python.org/downloads/release/python-376/)
* [Node 14](https://nodejs.org/)
* [Django 3](https://www.djangoproject.com/) 
* [Vue 2](https://vuejs.org) 
* [Redis](https://redis.io/) - установка [для windows](https://github.com/ServiceStack/redis-windows) или [docker](https://stackoverflow.com/a/62544583/10304212)

### Установка
```bash
# Убедиться, что активировано виртуальное окружение с python 3.7.6 и запущен redis

git clone https://gitlab.com/Murrengan/murr.git # копировать проект локально
pip install -r requirements/base.txt  # установка зависимостей python
python manage.py migrate # миграция (подготовка) базы данных
python manage.py prepare_stand_dev # создать тестовые данные
python manage.py create_white_emails # создать список проверенных почтовых ящиков

npm i # установка зависимостей node

```

### Запуск
```bash
# первая консоль 
python manage.py runserver
# вторая консоль
npm run serve
```

### Docker
```bash
# windows - для entrypoint.sh поставить line separator LF вместо CRLF (можно сменить в pycharm)

chmod +x entrypoint.sh # cделать файл entrypoint.sh исполняемым
docker-compose -f docker-compose-prod.yml up --build
```

###### Для https получить origin_ca_rsa_root.pem и private_origin_ca_ecc_root.pem сертификаты у cloudflare.com и разместить их в ./nginx/prod/conf.d

# 🌟Поддержать проект🌟 
[donationalerts](http://bit.do/eWnnm)

# ❤

## Контакты

[discord](https://discord.gg/gHFtAT3)

[youtube](https://youtube.com/murrengan/)

[telegram](https://t.me/MurrenganChat)

[vk](https://vk.com/murrengan)

[murrengan](https://www.murrengan.ru/)
