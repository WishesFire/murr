export default {
  state: {
    murrBattleRoomList: [],
    murrBattleRoom: null,
    showCreateMurrBattleRoomBtn: true
  },
  mutations: {
    setMurrBattleRoomList_mutations(state, murrBattleRoomData) {
      state.murrBattleRoomList.push(murrBattleRoomData);
    },
    setMurrBattleRoom_mutations(state, murrBattleRoomMurrensList) {
      const findRoom = (l) =>
        l.murr_websocket_id === murrBattleRoomMurrensList.murr_websocket_id;
      const indexRoom = state.murrBattleRoomList.findIndex(findRoom);
      const room = state.murrBattleRoomList[indexRoom];
      room.murrens_in_ws_data = murrBattleRoomMurrensList.murrens_in_ws_data;
      state.murrBattleRoomList[indexRoom] = room;
    },
    popMurrBattleRoom_mutations(state, murrBattleRoomId) {
      state.murrBattleRoomList.forEach((room, index) => {
        if (room.murr_websocket_id === murrBattleRoomId) {
          state.murrBattleRoomList.splice(index, 1);
        }
      });
    },
    setShowCreateMurrBattleRoomBtn_mutations(state, show) {
      state.showCreateMurrBattleRoomBtn = show
    }
  },
  getters: {
    getMurrBattleRoomList_getters(state) {
      return state.murrBattleRoomList;
    },
    getShowCreateMurrBattleRoomBtn_getters(state) {
      return state.showCreateMurrBattleRoomBtn
    }
  },
};
